package com.juggernaut.juggernaut.previewimage;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.juggernaut.juggernaut.R;
import com.juggernaut.juggernaut.basecode.AppData;
import com.juggernaut.juggernaut.basecode.Application;
import com.juggernaut.juggernaut.basecode.utilities.ImageBindingAdapter;

import javax.inject.Inject;

public class PreviewImage extends AppCompatActivity {

    ViewPager viewPager;
    MyCustomPagerAdapter myCustomPagerAdapter;

    @Inject
    AppData appData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_image);
        ((Application) getApplication()).getNetComponent().inject(this);

        viewPager = (ViewPager)findViewById(R.id.imageViewPreview);


        myCustomPagerAdapter = new MyCustomPagerAdapter(this, appData.getImageModelList());

        viewPager.setAdapter(myCustomPagerAdapter);


        viewPager.setCurrentItem(getIntent().getIntExtra("POSITION",0));

//        ImageBindingAdapter.loadImage(imageView,getIntent().getStringExtra("IMAGE_URL"));
      //  ImageBindingAdapter.loadImage(imageView,getIntent().getStringExtra("IMAGE_URL_LARGE"));
    }


}
