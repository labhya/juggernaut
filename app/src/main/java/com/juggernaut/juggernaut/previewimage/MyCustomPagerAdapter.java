package com.juggernaut.juggernaut.previewimage;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.juggernaut.juggernaut.R;
import com.juggernaut.juggernaut.basecode.utilities.ImageBindingAdapter;
import com.juggernaut.juggernaut.dashboard.ImageModel;

import java.util.List;

public class MyCustomPagerAdapter extends PagerAdapter {
    Context context;
    List<ImageModel> images;
    LayoutInflater layoutInflater;


    public MyCustomPagerAdapter(Context context, List<ImageModel> images) {
        this.context = context;
        this.images = images;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);



        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.picture_holder);
        requestOptions.error(R.drawable.picture_holder);

        Handler handler = new Handler();


        Glide.with(imageView.getContext())
                .setDefaultRequestOptions(requestOptions)
                .load(images.get(position).getPreviewURL())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                loadLargeImage(imageView,position,  resource);
                            }
                        });



                        return false;
                    }
                })
                .into(imageView);



  //      ImageBindingAdapter.loadImage(imageView,images.get(position).getPreviewURL());

//        imageView.setImageResource(images[position]);

        container.addView(itemView);

        //listening to image click
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //oast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG).show();
            }
        });

        return itemView;
    }


    void loadLargeImage(ImageView imageView, int position,Drawable resource)
    {

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(resource);
        requestOptions.error(resource);



        Glide.with(imageView.getContext())
                .setDefaultRequestOptions(requestOptions)
                .load(images.get(position).getLargeImageURL())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                        Log.d("TAG", "onResourceReady: ");



                        imageView.setImageDrawable(resource);

                        return false;
                    }
                })
                .into(imageView);


    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}