package com.juggernaut.juggernaut.juggernautdatabase;

import android.provider.BaseColumns;

public class JuggernautContract {

    public JuggernautContract() {
    }

    public static class Juggernaut implements BaseColumns{
        public static final String TABLE_IMAGE = "image_list";


        public static final String COLUMN_PREVIEW = "preview_image";
        public static final String COLUMN_PREVIEW_LARGE = "preview_image_large";
        public static final String COLUMN_IMAGE_ID = "image_id";
        public static final String COLUMN_UPDATED_AT = "updated_at";
        public static final String COLUMN_SEARCH_KEY = "search_key";

    }
}
