package com.juggernaut.juggernaut.juggernautdatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.juggernaut.juggernaut.dashboard.ImageModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.provider.BaseColumns._ID;
import static com.juggernaut.juggernaut.juggernautdatabase.JuggernautContract.Juggernaut.COLUMN_IMAGE_ID;
import static com.juggernaut.juggernaut.juggernautdatabase.JuggernautContract.Juggernaut.COLUMN_PREVIEW;
import static com.juggernaut.juggernaut.juggernautdatabase.JuggernautContract.Juggernaut.COLUMN_PREVIEW_LARGE;
import static com.juggernaut.juggernaut.juggernautdatabase.JuggernautContract.Juggernaut.COLUMN_SEARCH_KEY;
import static com.juggernaut.juggernaut.juggernautdatabase.JuggernautContract.Juggernaut.COLUMN_UPDATED_AT;
import static com.juggernaut.juggernaut.juggernautdatabase.JuggernautContract.Juggernaut.TABLE_IMAGE;

public class JuggerNautDatabaseHelper extends SQLiteOpenHelper{


    private static final int DATABASE_VERSION = 1;

    // Database Name
    public static final String DATABASE_NAME = "juggernautimageapp";


    public JuggerNautDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {


        String CREATE_RESTAURANT_TABLE = "CREATE TABLE " + TABLE_IMAGE + "("
                + _ID + " INTEGER PRIMARY KEY,"
                + COLUMN_PREVIEW + " TEXT ,"
                + COLUMN_SEARCH_KEY + " TEXT ,"
                + COLUMN_UPDATED_AT + " DOUBLE,"
                + COLUMN_IMAGE_ID + " TEXT NOT NULL UNIQUE,"
                + COLUMN_PREVIEW_LARGE + " TEXT " + ")";



        db.execSQL(CREATE_RESTAURANT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



    public long addRestaurant(ImageModel imageModel){

        SQLiteDatabase db = this.getWritableDatabase();
        long response = -1;
        ContentValues values = new ContentValues();

        values.put(COLUMN_IMAGE_ID,imageModel.getId());
        values.put(COLUMN_PREVIEW,imageModel.getPreviewURL());
        values.put(COLUMN_PREVIEW_LARGE, imageModel.getLargeImageURL());
        values.put(COLUMN_UPDATED_AT, Calendar.getInstance().getTime().getTime());
        values.put(COLUMN_SEARCH_KEY, imageModel.getSearchKey());


        if (CheckIsDataAlreadyInDBorNot(TABLE_IMAGE,COLUMN_IMAGE_ID,imageModel.getId()))
        {
            response = db.update(TABLE_IMAGE, values, COLUMN_IMAGE_ID + " = ?",
                    new String[] { String.valueOf(imageModel.getId()) });

        }else {
            response = db.insert(TABLE_IMAGE, null, values);

        }
        db.close(); // Closing database connection


        Log.d("DB", "addRestaurant: "+ response);

        return response;

    }


    public  boolean CheckIsDataAlreadyInDBorNot(String TableName,
                                                String dbfield, String fieldValue) {

        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select * from " + TableName + " where " + dbfield + " = " + "\"" + fieldValue + "\"";
        Cursor cursor = db.rawQuery(Query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }


    public List<ImageModel> getAllImageResultBySearchKey(String key)
    {

        List<ImageModel> imageModelList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();


        Cursor cursor = db.query(TABLE_IMAGE, null, COLUMN_SEARCH_KEY +" = ? ", new String[] { key }, null, null, COLUMN_UPDATED_AT+ " ASC");


        if (cursor != null)
            cursor.moveToFirst();




        do {

            if (cursor != null && cursor.getCount() != 0)
            {
                ImageModel imageModel = new ImageModel();

                imageModel.setLargeImageURL(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PREVIEW_LARGE)));
                imageModel.setPreviewURL(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PREVIEW)));
                imageModel.setId(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_IMAGE_ID)));
                imageModel.setSearchKey(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_SEARCH_KEY)));
                imageModel.setUpdatedAt(cursor.getDouble(cursor.getColumnIndexOrThrow(COLUMN_UPDATED_AT)));

                imageModelList.add(imageModel);


            }
        } while (cursor.moveToNext() );

        try{


        }catch (Exception e)
        {
            e.printStackTrace();
        }

        cursor.close();


        // Collections.reverse(restaurantList);



        return imageModelList;


    }

}
