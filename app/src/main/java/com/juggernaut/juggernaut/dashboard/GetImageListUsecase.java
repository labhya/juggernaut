package com.juggernaut.juggernaut.dashboard;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.juggernaut.juggernaut.basecode.Application;
import com.juggernaut.juggernaut.basecode.common.BlankData;
import com.juggernaut.juggernaut.basecode.common.UsecaseBase;
import com.juggernaut.juggernaut.basecode.repositories.JuggerNautRepositories;
import com.juggernaut.juggernaut.juggernautdatabase.JuggerNautDatabaseHelper;

import java.util.List;

import javax.inject.Inject;
import io.reactivex.Observable;


public class GetImageListUsecase extends UsecaseBase<GetImageListUsecase.Response> {

    private final String TAG = this.getClass().getSimpleName();

   // AppRepository appRepo;

    JuggerNautDatabaseHelper db;

    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    JuggerNautRepositories repositories;

    @Inject
    public GetImageListUsecase(JuggerNautRepositories repositories) {

        this.repositories = repositories;

        db = new JuggerNautDatabaseHelper(Application._getContext());

    }


    @Override
    public Observable<GetImageListUsecase.Response> buildObservable() {

        if (isNetworkAvailable())
        {
            Log.d(TAG, "buildObservable: 0");

            return repositories.getImageList(key);
        }else {

            Log.d(TAG, "buildObservable: 1");

            Response response = new Response();

            response.setHits(db.getAllImageResultBySearchKey(key));

            return Observable.just(response);

        }

    }

    public static class Response {

        private List<ImageModel> hits;

        public List<ImageModel> getHits() {
            return hits;
        }

        public void setHits(List<ImageModel> hits) {
            this.hits = hits;
        }
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) Application._getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}