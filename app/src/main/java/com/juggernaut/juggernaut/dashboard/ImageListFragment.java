package com.juggernaut.juggernaut.dashboard;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.juggernaut.juggernaut.R;
import com.juggernaut.juggernaut.basecode.AppData;
import com.juggernaut.juggernaut.basecode.Application;
import com.juggernaut.juggernaut.basecode.common.BaseBindingAdapter;
import com.juggernaut.juggernaut.basecode.common.BaseFragment;
import com.juggernaut.juggernaut.databinding.SingleRecyclerViewLayoutBinding;
import com.juggernaut.juggernaut.previewimage.PreviewImage;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class ImageListFragment extends BaseFragment implements ImageListContract.View, BaseBindingAdapter.Listener<ImageModel>, SearchView.OnQueryTextListener {

    private final String TAG = this.getClass().getSimpleName();

    SingleRecyclerViewLayoutBinding binding;
    ImageListAdapter adapter;
    List<ImageModel> list;

    GridLayoutManager layoutManager;

    @Inject
    AppData appData;

    @Inject
    ImageListPresenter presenter;

    public static ImageListFragment getInstance(Bundle bundle) {
        ImageListFragment fragment = new ImageListFragment();

        if (bundle == null) {
            bundle = new Bundle();
        }

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = SingleRecyclerViewLayoutBinding.inflate(inflater, container, false);


        adapter = new ImageListAdapter(getActivity(), new ArrayList<>(), this);
        adapter.setEmptyView(binding.includedEmptyView.emptyView);

        layoutManager = new GridLayoutManager(getContext(), 2);




        binding.recyclerView.setLayoutManager(layoutManager);


        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.recyclerView.setAdapter(adapter);


        presenter.attachView(this);

        binding.swipeRefresh.setOnRefreshListener(() -> {

            binding.swipeRefresh.setRefreshing(false);
        });



        presenter.getImages("");


        return binding.getRoot();
    }

    void updateGridSpanCount(int count){
        layoutManager.setSpanCount(count);
    }

    void getImageList(String str){

        startAnimation();
        presenter.getImages(str);
    }

    public void refresh() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((Application) getActivity().getApplication()).getNetComponent().inject(this);
    }

    @Override
    public void postErrorHandling() {

        stopAnimation();
    }

    @Override
    public View getLoader() {
        return null;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }



    public void close() {

        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return true;
    }

    @Override
    public void onItemClick(View view, ImageModel item, int clickType) {


        String transitionName = getString(R.string.transition_string);

        ActivityOptionsCompat options =

                ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                        view,   // Starting view
                        transitionName    // The String
                );

        Intent intent = new Intent(getActivity(), PreviewImage.class);
       // intent.putExtra("IMAGE_URL",item.getPreviewURL());
       // intent.putExtra("IMAGE_URL_LARGE",item.getLargeImageURL());



        if (appData != null && appData.getImageModelList() != null && appData.getImageModelList().size() != 0)
            intent.putExtra("POSITION",appData.getImageModelList().indexOf(item));

        startActivity(intent,options.toBundle());

    }


    @Override
    public void refreshList(List<ImageModel> imageModels) {

        stopAnimation();

        appData.setImageModelList(imageModels);

        adapter.swapItems(imageModels);
    }


    void startAnimation(){
        binding.blankView.setVisibility(View.VISIBLE);

        binding.blankView1.shimmer.startShimmerAnimation();
        binding.blankView2.shimmer.startShimmerAnimation();
        binding.blankView3.shimmer.startShimmerAnimation();
        binding.blankView4.shimmer.startShimmerAnimation();
    }

    void stopAnimation(){
        binding.blankView.setVisibility(View.GONE);

        binding.blankView1.shimmer.stopShimmerAnimation();
        binding.blankView2.shimmer.stopShimmerAnimation();
        binding.blankView3.shimmer.stopShimmerAnimation();
        binding.blankView4.shimmer.stopShimmerAnimation();
    }

}




