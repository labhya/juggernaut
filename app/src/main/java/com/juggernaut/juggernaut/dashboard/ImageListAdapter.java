package com.juggernaut.juggernaut.dashboard;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;


import com.juggernaut.juggernaut.basecode.common.BaseBindingAdapter;
import com.juggernaut.juggernaut.databinding.ListItemImageBinding;

import java.util.List;


public class ImageListAdapter extends BaseBindingAdapter<ListItemImageBinding, ImageModel> {

    private final String TAG = this.getClass().getSimpleName();
    int mode = -1;

    public ImageListAdapter(Context context, List<ImageModel> list, BaseBindingAdapter.Listener listener) {
        super(context, list, listener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListItemImageBinding binding = ListItemImageBinding.inflate(mInflater, parent, false);
        return new ImageListViewHolder(binding, listener);
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    protected String getPrimaryId(ImageModel item) {
        return null;
    }

    public class ImageListViewHolder extends BaseViewHolder<ListItemImageBinding, ImageModel> {

        public ImageListViewHolder(ListItemImageBinding binding, Listener l) {
            super(binding, l);
        }

        @Override
        public void bind(ImageModel model, int mode) {


            binding.setModel(model);
            binding.setListener(listener);

            Log.d(TAG, "bind: " + mode);

            if (getMode() != -1) {
                mode = getMode();
            }
            binding.setMode(mode);


        }
    }


}