package com.juggernaut.juggernaut.dashboard;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.juggernaut.juggernaut.basecode.Application;
import com.juggernaut.juggernaut.basecode.common.BasePresenterImpl;
import com.juggernaut.juggernaut.basecode.common.BaseView;
import com.juggernaut.juggernaut.basecode.common.BlankData;
import com.juggernaut.juggernaut.juggernautdatabase.JuggerNautDatabaseHelper;

import java.util.List;

import javax.inject.Inject;



public class ImageListPresenter extends BasePresenterImpl implements ImageListContract.Presenter {

    private final String TAG = this.getClass().getSimpleName();
    ImageListContract.View view;

    GetImageListUsecase usecase;

    JuggerNautDatabaseHelper db;

    @Inject
    ImageListPresenter(GetImageListUsecase usecase) {

        this.usecase = usecase;

        db = new JuggerNautDatabaseHelper(Application._getContext());


    }

    @Override
    public BaseView getView() {
        return view;
    }

    @Override
    public void attachView(BaseView v) {
        this.view = (ImageListContract.View) v;
    }

    @Override
    public void getImages(String str) {
        usecase.setKey(str);

        usecase.execute().subscribe(this::onSuccess, this::onError);
    }

    private void onSuccess(GetImageListUsecase.Response response) {


        if (response != null && response.getHits() != null)
        {
            for (ImageModel imageModel : response.getHits())
            {

                imageModel.setSearchKey(usecase.getKey());
                db.addRestaurant(imageModel);

            }

        }


        view.refreshList(response.getHits());
    }


}
