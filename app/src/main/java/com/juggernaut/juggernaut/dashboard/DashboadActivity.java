package com.juggernaut.juggernaut.dashboard;

import android.app.SearchManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.juggernaut.juggernaut.R;
import com.juggernaut.juggernaut.basecode.utilities.ActivityUtility;

public class DashboadActivity extends AppCompatActivity {

    ImageListFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboad);

        fragment = ImageListFragment.getInstance(getIntent().getExtras());

        ActivityUtility.addFragment(this,fragment,R.id.containerList,false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));



        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                fragment.getImageList(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.one: {
                fragment.updateGridSpanCount(2);
                // do your sign-out stuff
                break;
            }
            case R.id.two: {
                fragment.updateGridSpanCount(3);
                // do your sign-out stuff
                break;
            }
            case R.id.three: {
                fragment.updateGridSpanCount(4);
                // do your sign-out stuff
                break;
            }
            // case blocks for other MenuItems (if any)
        }

        return false;
    }
}
