package com.juggernaut.juggernaut.dashboard;


import com.juggernaut.juggernaut.basecode.common.BasePresenter;
import com.juggernaut.juggernaut.basecode.common.BaseView;

import java.util.List;

public interface ImageListContract {

    public interface View extends BaseView {

        void refreshList(List<ImageModel> imageModels);
    }

    interface Presenter extends BasePresenter {

        void getImages(String str);
    }
}