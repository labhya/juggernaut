package com.juggernaut.juggernaut.basecode.common;

public interface BasePresenter {

    void attachView(BaseView v);
    void onError(Throwable e);
    void onDestroy();

}
