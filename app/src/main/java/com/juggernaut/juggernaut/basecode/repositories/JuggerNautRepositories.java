package com.juggernaut.juggernaut.basecode.repositories;


import com.juggernaut.juggernaut.basecode.common.BlankData;
import com.juggernaut.juggernaut.dashboard.GetImageListUsecase;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Query;

public interface JuggerNautRepositories {

   // Observable<GetRestaurantListUsecase.Response> getListRestaurant(String latlng);

    Observable<GetImageListUsecase.Response> getImageList(@Query("q") String search);
}
