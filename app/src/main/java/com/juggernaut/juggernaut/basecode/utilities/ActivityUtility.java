package com.juggernaut.juggernaut.basecode.utilities;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.juggernaut.juggernaut.basecode.common.BaseFragment;


public class ActivityUtility {

    public static void addFragment(AppCompatActivity ref, BaseFragment fragment, int containerId,
                                   boolean addToBackStack) {
        FragmentTransaction ft = ref.getSupportFragmentManager().beginTransaction();
        ft.add(containerId, fragment, fragment.getClass().getSimpleName());
        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        ft.commit();
    }
}
