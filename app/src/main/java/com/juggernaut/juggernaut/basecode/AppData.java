package com.juggernaut.juggernaut.basecode;

import com.juggernaut.juggernaut.dashboard.ImageModel;

import java.util.List;

public class AppData {

    private List<ImageModel> imageModelList;

    public List<ImageModel> getImageModelList() {
        return imageModelList;
    }

    public void setImageModelList(List<ImageModel> imageModelList) {
        this.imageModelList = imageModelList;
    }
}
