package com.juggernaut.juggernaut.basecode.network;


import com.juggernaut.juggernaut.basecode.common.BlankData;
import com.juggernaut.juggernaut.dashboard.GetImageListUsecase;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JuggerNautAppService {


    @GET(".")
    Observable<GetImageListUsecase.Response> getImageList(@Query("key") String key, @Query("q") String search);

}
