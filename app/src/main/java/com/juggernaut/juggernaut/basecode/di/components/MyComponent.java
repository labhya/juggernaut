package com.juggernaut.juggernaut.basecode.di.components;



import com.juggernaut.juggernaut.basecode.common.BaseFragment;
import com.juggernaut.juggernaut.basecode.di.modules.MyModule;
import com.juggernaut.juggernaut.basecode.di.modules.NetModule;
import com.juggernaut.juggernaut.dashboard.DashboadActivity;
import com.juggernaut.juggernaut.dashboard.ImageListFragment;
import com.juggernaut.juggernaut.previewimage.PreviewImage;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import io.reactivex.Scheduler;

@Singleton
@Component(modules = {NetModule.class, MyModule.class })
public interface MyComponent {

//    void inject(RestaurantListFragment restaurantListFragment);
    void inject(BaseFragment baseFragment);
    void inject(DashboadActivity dashboardActivity);
    void inject(ImageListFragment imageListFragment);
    void inject(PreviewImage previewImage);
//    void inject(MsgDetailsListFragment msgDetailsListFragment);


//    Retrofit retrofit();
//    OkHttpClient okHttpClient();
//
//
    @Named("ui_thread")
    Scheduler uiThread();
    @Named("executor_thread")
    Scheduler executorThread();

}