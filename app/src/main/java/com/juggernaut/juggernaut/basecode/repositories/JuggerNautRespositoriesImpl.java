package com.juggernaut.juggernaut.basecode.repositories;


import com.juggernaut.juggernaut.basecode.Constants;
import com.juggernaut.juggernaut.basecode.common.BlankData;
import com.juggernaut.juggernaut.basecode.network.JuggerNautAppService;
import com.juggernaut.juggernaut.dashboard.GetImageListUsecase;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class JuggerNautRespositoriesImpl implements JuggerNautRepositories {


    private final String TAG = this.getClass().getSimpleName();

    private final Scheduler mUiThread;
    private final Scheduler mExecutorThread;
    private JuggerNautAppService juggerNautAppService;

    @Inject
    public JuggerNautRespositoriesImpl(JuggerNautAppService juggerNautAppService,
                                       @Named("ui_thread") Scheduler uiThread,
                                       @Named("executor_thread") Scheduler executorThread)
    {
        this.mUiThread = uiThread;
        this.mExecutorThread = executorThread;
        this.juggerNautAppService = juggerNautAppService;

    }

    @Override
    public Observable<GetImageListUsecase.Response> getImageList(String search) {
        return juggerNautAppService.getImageList(Constants.API_KEY,search).subscribeOn(mExecutorThread).observeOn(mUiThread);
    }

//    @Override
//    public Observable<GetRestaurantListUsecase.Response> getListRestaurant(String latlng) {
//
////        return null;
//         return fueledAppService.getListRestaurant(Constants.AUTH,latlng,Constants.CATEGORY_FOOD,Constants.VERSION).subscribeOn(mExecutorThread).observeOn(mUiThread);
//    }
}
