package com.juggernaut.juggernaut.basecode;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.juggernaut.juggernaut.basecode.di.components.DaggerMyComponent;
import com.juggernaut.juggernaut.basecode.di.components.MyComponent;
import com.juggernaut.juggernaut.basecode.di.modules.MyModule;
import com.juggernaut.juggernaut.basecode.di.modules.NetModule;


public class Application extends MultiDexApplication {


    private static Application instance;

    private MyComponent netComponent;




    @Override
    public void onCreate() {

        super.onCreate();

        instance = this;
        netComponent = DaggerMyComponent.builder()
                .myModule(new MyModule(this))
                .netModule(new NetModule(Constants.BASE_URL))
                .build();

        
    }
    public static Context _getContext(){
        return instance;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public MyComponent getNetComponent(){return  netComponent;}


}
