package com.juggernaut.juggernaut.basecode.common;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseDiffAdapter<I> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    protected List<I> list = new ArrayList<I>();

    private View emptyView;


    protected void checkItems(){
        if(emptyView != null){

            if(list.size() == 0){
                emptyView.setVisibility(View.VISIBLE);
            }else{
                emptyView.setVisibility(View.GONE);
            }

        }

    }


    public void swapItems(List<I> actors) {
        final DiffUtil.Callback diffCallback = getCallback(actors);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.list.clear();

        this.list.addAll(actors);
        diffResult.dispatchUpdatesTo(this);
        checkItems();
    }












    protected abstract String getPrimaryId(I item);

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    protected abstract DiffUtil.Callback getCallback(List<I> actors);


    public View getEmptyView() {
        return emptyView;
    }

    public void setEmptyView(View emptyView) {
        this.emptyView = emptyView;
    }


}
