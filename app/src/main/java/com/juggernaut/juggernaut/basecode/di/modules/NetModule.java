package com.juggernaut.juggernaut.basecode.di.modules;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.juggernaut.juggernaut.basecode.AppData;
import com.juggernaut.juggernaut.basecode.network.JuggerNautAppService;
import com.juggernaut.juggernaut.basecode.repositories.JuggerNautRepositories;
import com.juggernaut.juggernaut.basecode.repositories.JuggerNautRespositoriesImpl;
import com.juggernaut.juggernaut.dashboard.DashboadActivity;

import java.util.Collections;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetModule {

    String baseUrl;

    // Constructor needs one parameter to instantiate.
    public NetModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    @Singleton
    DashboadActivity providesDashboard() {
        return new DashboadActivity();
    }


    @Provides
    @Singleton
    AppData providesAppData() {
        return new AppData();
    }

    // Dagger will only look for methods annotated with @Provides
    @Provides
    @Singleton
    SharedPreferences providesSharedPreferences(Application application) {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }


    @Provides
    @Singleton
    Cache provideOkHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(application.getCacheDir(), cacheSize);
        return cache;
    }



    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Cache cache) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        ConnectionSpec spec = new
                ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_RSA_WITH_AES_256_GCM_SHA384,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
                        CipherSuite.TLS_ECDHE_RSA_WITH_RC4_128_SHA,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
                )
                .build();

        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client;
            client = new OkHttpClient.Builder().addInterceptor(interceptor).cache(cache).
                    connectionSpecs(Collections.singletonList(spec)).build();


        return client;
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit;
    }



    @Provides
    @Singleton
    JuggerNautAppService provideFueledAppService(Retrofit retrofit)
    {
        return retrofit.create(JuggerNautAppService.class);
    }


    @Provides
    @Singleton
    JuggerNautRepositories provideFueledRepositories(JuggerNautRespositoriesImpl juggerNautRespositories){

        return juggerNautRespositories;
    }


}
